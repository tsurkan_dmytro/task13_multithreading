package epam.task.multithreading.pingponglock;

class PrintingJob implements Runnable
{
    private DataReader dataReader;

    public PrintingJob(DataReader dataReader)
    {
        this.dataReader = dataReader;
    }

    @Override
    public void run()
    {
        System.out.printf("%s: Going to print a document\n", Thread.currentThread().getName());
        dataReader.printJob(new Object());
    }
}