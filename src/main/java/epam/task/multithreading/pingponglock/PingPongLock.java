package epam.task.multithreading.pingponglock;

public class PingPongLock {

    public PingPongLock() {
        DataReader dataReader = new DataReader();
        Thread thread[] = new Thread[10];
        for (int i = 0; i < 10; i++)
        {
            thread[i] = new Thread(new PrintingJob(dataReader), "Thread " + i);
        }
        for (int i = 0; i < 10; i++)
        {
            thread[i].start();
        }
    }
}
