package epam.task.multithreading.blockingqueueex;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class MainBlocking {

    public MainBlocking() {
        BlockingQueue<Integer> queue = new LinkedBlockingQueue<>(9);

        new Thread(new Producer(queue)).start();
        new Thread(new Consumer(queue)).start();

    }
}
