package epam.task.multithreading.blockingqueueex;

import java.util.concurrent.BlockingQueue;

public class Producer implements Runnable {

    private final BlockingQueue<Integer> queue;

    public Producer(BlockingQueue<Integer> queue) {
        this.queue = queue;
    }

    @Override
    public void run() {
        try {
            proccess();
        }catch (InterruptedException e){
            Thread.currentThread().interrupt();
        }
    }

    private void proccess() throws InterruptedException {

        for (int i = 0; i < 20; i++){
            System.out.println(" [Producer] Put : " + i);
            queue.put(i);
            System.out.println("[Produser] Queue remainingCapacity : " + queue.remainingCapacity());
            Thread.sleep(100);
        }
    }
}
