package epam.task.multithreading.rwlock;

public class ReadLock{

    public synchronized void lock() {
        /*
         * More than one threads can acquire readLock at a time, provided
         * no other thread is acquiring writeLock at same time.
         */
        if(writeLockCount==0){
            readLockCount++;
        }
        /*
         * if some other thread is
         * acquiring write lock at that time,
         * than current thread waits.
         */
        else{
            try {
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public synchronized void unlock() {
        readLockCount--; //decrement readLockCount.

        /*
         * If readLockCount has become 0,
         * all threads waiting to write will be notified
         * and can acquire lock.
         */
        if(readLockCount==0)
            notify();
    }

}