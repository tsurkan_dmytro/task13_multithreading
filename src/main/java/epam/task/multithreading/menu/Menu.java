package epam.task.multithreading.menu;

import epam.task.multithreading.blockingqueueex.MainBlocking;
import epam.task.multithreading.fibonacci.FibonacciExec;
import epam.task.multithreading.pingponglock.PingPongLock;
import epam.task.multithreading.pingpongsync.PingPong;
import epam.task.multithreading.pipes.PipeLx;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class Menu {

    private Map<String,String> menu;
    private Map<String,Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);

    public Menu() {
        menu = new LinkedHashMap<>();
        methodsMenu = new LinkedHashMap<>();

        menu.put("1", " 1 - showPipe");
        menu.put("2", " 2 - showPingPong");
        menu.put("3", " 3 - showPingPongLock");
        menu.put("4", " 4 - showFibonacciExec");
        menu.put("5", " 5 - showMainBlocking");
        menu.put("Q", " Q - exit");

        methodsMenu.put("1", this::showPipe);
        methodsMenu.put("2", this::showPingPong);
        methodsMenu.put("3", this::showPingPongLock);
        methodsMenu.put("4", this::showFibonacciExec);
        methodsMenu.put("5", this::showMainBlockingQueue);

    }

    private void outputMenu(){
        System.out.println("\n MENU:");

        for (String str: menu.values()){
            System.out.println(str);
        }
    }

    public void show(){
        String keyMenu = null;

        do {
            outputMenu();
            System.out.println("Select menu: ");
            keyMenu = input.nextLine().toUpperCase();
            try{
                methodsMenu.get(keyMenu).print();
            }catch (Exception e){
                System.out.println("menu problem");
            }
        }while (!keyMenu.equals("Q"));
    }


    private void showPipe(){
        PipeLx pipeEx = new PipeLx();
    }

    private void showPingPong(){
        PingPong pingPong = new PingPong();
    }
    private void showPingPongLock(){
        PingPongLock pongLock = new PingPongLock();
    }
    private void showFibonacciExec(){
        FibonacciExec fibonacci = new FibonacciExec();
    }

    private void showMainBlockingQueue() {
        MainBlocking mainBlocking = new MainBlocking();
    }
}