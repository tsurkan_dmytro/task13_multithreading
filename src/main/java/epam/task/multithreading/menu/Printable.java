package epam.task.multithreading.menu;

@FunctionalInterface
public interface Printable {
    public void print();
}
