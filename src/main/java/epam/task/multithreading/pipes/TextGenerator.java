package epam.task.multithreading.pipes;

import java.io.IOException;
import java.io.Writer;

class TextGenerator extends Thread {
    private Writer out;

    public TextGenerator(Writer out){

            this.out = out;
        }
    
    public void run () {
            
        try {
            try {

                for (char с = 'A'; с <= 'Z'; с++)
                        out.write(с+" ");
            } finally {
                out.close();
            }

        } catch (IOException e) {
            getThreadGroup().uncaughtException(this, e);
        }
    }
}

