package epam.task.multithreading.pipes;

import java.io.IOException;
import java.io.PipedReader;
import java.io.PipedWriter;

public class PipeLx {

    public PipeLx()  {


        PipedWriter out = new PipedWriter();
        PipedReader in = null;
        try {
            in = new PipedReader(out);

        TextGenerator data = new TextGenerator(out);

        data.start();

        int ch;

        while ((ch = in.read()) != -1)

            System.out.print((char) ch);

        System.out.println() ;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}